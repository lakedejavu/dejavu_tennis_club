<?php require_once 'layouts/header.php'; ?>

    <!-- Content-->
    <div class="container">
        <div class="section row center" id="top">
            <h1>About Us</h1>
            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro qui cum ad libero vitae tempora quidem eveniet facere. Adipisci molestias quos hic corrupti quidem in illo libero pariatur exercitationem.</p>
        </div>
       
          <h3 class="indigo-text center">Photo Ablum</h3>
          <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias porro qui cum ad libero vitae tempora quidem eveniet facere. Adipisci molestias quos hic corrupti quidem in illo libero pariatur exercitationem.</p>
          <div class="row flex-container">
              <div><img class="materialboxed img-responsive" src="images/rf.jpg" width="180" style="object-fit: cover;"></div>
              <div><img class="materialboxed" src="images/tennis-2086224_1920.jpg" width="180"></div>
              <div><img class="materialboxed" src="images/kim-clijsters-288567_1920.jpg" width="180"></div>
              <div><img class="materialboxed img-responsive" src="images/rafael-nadal-288554_1920.jpg" width="180"></div>
              <div><img class="materialboxed" src="images/tennis-player-676315_1920.jpg" width="180"></div>
              <div><img class="materialboxed" src="images/kim-clijsters-288567_1920.jpg" width="180"></div>   
          </div>
       

    </div>

    <!--Footer-->
    <?php include 'layouts/footer.php'; ?>