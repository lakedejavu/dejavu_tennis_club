<?php require_once 'layouts/header.php'; ?>
    <!--main content- weeklyRR-->
    <div class="container">
        <div class="row">
            <div class="col sm12 m12" left>
            <h1 class="indigo-text">Weekly Round Robins</h1>
            <p class="grey-text text-darken-3">Each Week, each member is required to play round robins.<br> 
               First, notify us your attendance then club co-ordinator will schedule courts and assign each member to them.
               <br>By the result of each week, members can move up or down levels following week.
            </p>
            </div>
        </div>
        <div class="row">
            <!-- EXISTING MEMBERS -->
            <ul class="pagination">
                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">2</a></li>
                <li class="waves-effect"><a href="#!">3</a></li>
                <li class="waves-effect"><a href="#!">4</a></li>
                <li class="waves-effect"><a href="#!">5</a></li>
                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </ul>
            <div class="col s10 m6"> <h5 class="indigo-text">Existing Members</h5>             
                <div class="collection" id="members">                    
                </div>                                            
            </div>
            
            <!-- ATTENDING LIST -->           
               
            <div class="col s12 m5 right">
                <h5 class="indigo-text">This week's attending players</h5>            
                <ul class="collection indigo" id="attendees">
                    <!-- <a href="#!" class="collection-item indigo-text"></a> -->
                </ul>
            </div>            
        </div>     
        
           

    </div><!--container-->

    <!--Footer-->
<?php include 'layouts/footer.php'; ?>