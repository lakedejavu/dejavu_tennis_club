<?php
    require_once './connection.php';

    try{
        if(isset($_REQUEST["term"])){
            // create prepared statement
            $sql = "SELECT * FROM members WHERE firstname LIKE :term";
            $stmt = $conn->prepare($sql);
            $term = $_REQUEST["term"] . '%';
            // bind parameters to statement
            $stmt->bindParam(":term", $term);
            // execute the prepared statement
            $stmt->execute();
            if($stmt->rowCount() > 0){
                while($row = $stmt->fetch()){
                    echo "<p>" . $row["firstname"] . " ".$row["lastname"]. "</p>";
                }
            } else{
                echo "<p>No matches found</p>";
            }
        }  
    } catch(PDOException $e){
        die("ERROR: Could not able to execute $sql. " . $e->getMessage());
    }
     
    // Close statement
    unset($stmt);
     
    // Close connection
    unset($conn);

?>
