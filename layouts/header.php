<!DOCTYPE html>
  <html>
    <head>        
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
      <link rel="stylesheet" href="./css/styles.css">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      
    </head>

    <body>
    <header>
      <nav>
        <div class="nav-wrapper indigo">
        <a href="../index.php" class="brand-logo center">Dejavu Tennis Club</a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">               
          <li><a href="about.php">About Our Club</a></li>
          <li><a href="news.php">News</a></li>
          <li><a href="#fees">Schedules & Fees</a></li>
          <li><a href="weeklyRR.php">Weekly RR</a></li>
          <li><a href="#contact">Contact Us</a></li>
          <li><a href="#signin"><i class="material-icons left">supervisor_account</i>Sign-In</a></li>     
        </ul>
        </div>
      </nav>
      <ul class="sidenav" id="mobile-demo">
          <li><a href="../index.php">Dejavu Tennis Club</a></li>
          <li><a href="#signin"><i class="material-icons left">supervisor_account</i>Sign-In</a></li>
          <li><a href="about.php">About Our Club</a></li>
          <li><a href="news.php">News</a></li>
          <li><a href="#fees">Schedules & Fees</a></li>
          <li><a href="weeklyRR.php">Weekly RR</a></li>
          <li><a href="#contact">Contact Us</a></li>              
      </ul>      
    </header> 