    <!--Footer-->
    <footer class="page-footer indigo" id="contact">
            <div class="container">
              <div class="row">
                <div class="col l6 s12">
                  <h5 class="white-text"><i class="material-icons left">location_on</i>Find Us</h5>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2878.7039459810635!2d-79.35131508429805!3d43.820499949613804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4d36489692689%3A0xf8655309ce0fcbbe!2s50+Steelcase+Rd+E%2C+Markham%2C+ON+L3R+1E8!5e0!3m2!1sen!2sca!4v1552768647199" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe> 
                </div>
                <div class="col l4 offset-l2 s12">
                  <h5 class="white-text">Contact Information</h5>
                  <ul>
                    <li><a class="grey-text text-lighten-3" href="#!"><i class="material-icons left">email</i>Email: francis.moon@gmail.com</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!"><i class="material-icons left">phone_android</i>Phone: 647-xxx-xxxx</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="footer-copyright center">
              <div class="container">
              © 2019 Copyright Dejavu Tennis Club
              </div>
            </div>
          </footer>
          <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="./js/init.js"></script>
      </body>
      </html>