$(document).ready(function(){
    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.collapsible').collapsible();
    $('.materialboxed').materialbox();

 

    //news
    $.getJSON('./js/news.json', function(data){
        var listOutput = '';
       
        $.each(data, function(index, val){
            
            var title = val.title;
            var date = val.date;
            var content = val.content;
            listOutput = '<li><div class="collapsible-header indigo-text"><h5>' 
            + title + '<br><small class="grey-text">' 
            + date + '</small></h5></div><div class="collapsible-body"><blockquote blue>'
             + content + '</blockquote></div></li>';
            $('#newslist').append(listOutput);
            
        });
        $('.collapsible').collapsible('open', 0);
    });
    
    
    //Weekly RR
    $.getJSON('./js/data.json', function(data){        
    
    var output= '';
    var attendees = new Array();
    var memberObj = {};
   
     for (var i=0; i< data.length; i++)  { 
        memberObj = {name: data[i].name, ntrp: data[i].ntrp};
        output = '<a class="collection-item avatar indigo-text" href="#!" id="'+memberObj['name'] + ' ' + memberObj['ntrp'] +'"><i class="material-icons circle indigo">person</i>' + memberObj['name'] + ' ' + memberObj['ntrp'] +'<i class="secondary-content material-icons indigo-text">add</i></a>';
          
        $('#members').append(output);
      }
      
        //click to add to attendes array and move to #attendees
        // Then removes data from data
        $('#members').on('click', 'a', function(){ 
            if($(this).attr('id')) { 
                console.log($(this).text());          
                $('#attendees').append('<li class="collection-item">' + $(this).attr('id') + ' <i class="material-icons right deep-orange-text">check_circle</i></li>');
            }
            // console.log($(this).attr('id') + ' is clicked');
        });

    });
   
    
    
});
