<?php require 'layouts/header.php';?>
     
    <!-- Page Content goes here -->
    <div class="section parallax-container center valign-wrapper">
      <div class="container">
        <div class="row">
          <div class="col s12 white-text">
            <h2 class="white-text">Have Fun with Us!</h2>
            <p class="white-text darken-4">I am a very simple card. I am good at containing small bits of information.
      I am convenient because I require little markup to use effectively.</p>
          </div>
        </div>
      </div>
      <div class="parallax"><img calss="responsive-img" src="images/image-505758948.jpg"></div>
    </div>
    </div>  
            
    <div class="container">      
      <div class="section row">
        <div class="col m4 s12 hide-on-small-only">
          <!-- Promo Content 1 goes here -->
          <div class="card-panel hoverable indigo">
              <h3 class="indigo-text text-lighten-2">Check Your Membership</h3>
              <p class="white-text">I am a very simple card. I am good at containing small bits of information.
                  I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.
              </p>
              <button class="btn white indigo-text">Let's go</button>              
          </div>
        </div>
        <div class="col m4 s12 hide-on-small-only">
          <!-- Promo Content 2 goes here -->
          <div class="card-panel hoverable indigo">
            <h3 class="indigo-text text-lighten-2">Weekly Round Robins</h3>
            <p class="white-text">I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.
            </p>
            <button class="btn white indigo-text">Let's go</button>              
          </div>
        </div>
        <div class="col m4 s12 hide-on-small-only">
          <!-- Promo Content 3 goes here -->
          <div class="card-panel hoverable indigo">
            <h3 class="indigo-text text-lighten-2">Who's playing this week</h3>
            <p class="white-text">I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.
            </p>
            <button class="btn white indigo-text">Let's go</button>
          </div>
        </div>    
      </div>
      <!-- Fees -->
      <div class="section row center" id="fees">
        <h2 class="indigo-text darken-4">Club Fees & Schedules</h2>
        <table class="striped responsive-table">
          <thead>
          <tr>
            <th>1-day/week</th>
            <th>2-day/week</th>
            <th>Additional Play per Week</th>
            <th>Guest - Non-Mayfair Club Member</th>
            <th>Guest - Mayfair Club member</th>
            <th>Court Schedules</th>           
          </tr>
          </thead>
        <tbody>
          <tr>
            <td>$60/month</td>
            <td>$115/month</td>
            <td>$15/visit</td>
            <td>$25/visit</td>
            <td>$20/month</td>
            <td>M/F 8:30pm-11:30pm</td>
          </tr>
        </tbody> 
        </table>
        <p>* If you'd like to join the club, please contact us for more information.</p>
      </div>
      <!-- Club News -->
      <div class="section row center" id="news">
        <h2 class="indigo-text darken-4">Club News</h2>
        <ul class="collapsible">
            <li class="active">
              <div class="collapsible-header"><i class="material-icons">filter_drama</i>First <span class="new badge right"></span></div>
              <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
            </li>
            <li>
              <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
              <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
            </li>
            <li>
              <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
              <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
            </li>
        </ul>
      </div>
    </div><!--container-->
    <!--Footer-->
    <footer class="page-footer indigo darken-4" id="contact">
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text"><i class="material-icons left">location_on</i>Find Us</h5>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2878.7039459810635!2d-79.35131508429805!3d43.820499949613804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4d36489692689%3A0xf8655309ce0fcbbe!2s50+Steelcase+Rd+E%2C+Markham%2C+ON+L3R+1E8!5e0!3m2!1sen!2sca!4v1552768647199" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe> 
            </div>
            <div class="col l4 offset-l2 s12">
              <h5 class="white-text">Contact Information</h5>
              <ul>
                <li><a class="grey-text text-lighten-3" href="#!"><i class="material-icons left">email</i>Email: francis.moon@gmail.com</a></li>
                <li><a class="grey-text text-lighten-3" href="#!"><i class="material-icons left">phone_android</i>Phone: 647-xxx-xxxx</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright center">
          <div class="container">
          © 2019 Copyright Dejavu Tennis Club
          </div>
        </div>
      </footer>
      <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="./js/init.js"></script>
  </body>
  </html>
        